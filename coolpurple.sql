-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 18, 2018 at 08:23 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coolpurple`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `adminID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`adminID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `classcustomer`
--

DROP TABLE IF EXISTS `classcustomer`;
CREATE TABLE IF NOT EXISTS `classcustomer` (
  `customerID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) COLLATE utf8_bin NOT NULL,
  `lastName` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`customerID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `classcustomer`
--

INSERT INTO `classcustomer` (`customerID`, `firstName`, `lastName`) VALUES
(1, 'Testy', 'Ickles'),
(2, 'Trottle', 'Bee');

-- --------------------------------------------------------

--
-- Table structure for table `classorder`
--

DROP TABLE IF EXISTS `classorder`;
CREATE TABLE IF NOT EXISTS `classorder` (
  `orderNR` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `total` double NOT NULL,
  `customerID` int(11) NOT NULL,
  PRIMARY KEY (`orderNR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `classproduct`
--

DROP TABLE IF EXISTS `classproduct`;
CREATE TABLE IF NOT EXISTS `classproduct` (
  `productID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` longtext COLLATE utf8_bin NOT NULL,
  `startingStock` int(11) NOT NULL,
  `endingStock` int(11) NOT NULL,
  `purchasePrice` double NOT NULL,
  `sellingPrice` double NOT NULL,
  `minStock` int(11) NOT NULL,
  `maxStock` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  PRIMARY KEY (`productID`)
) ENGINE=MyISAM AUTO_INCREMENT=10003 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `classproduct`
--

INSERT INTO `classproduct` (`productID`, `name`, `description`, `startingStock`, `endingStock`, `purchasePrice`, `sellingPrice`, `minStock`, `maxStock`, `categoryID`) VALUES
(1001, 'Gigabyte 1070OC', 'Gigabytes version of the NVidea 1070 overclocked bla bla', 5, 2, 320, 649, 3, 10, 0),
(1002, 'Samsung gear4 pro', 'The newest smartwatch from Samsung', 2, 1, 160, 399, 2, 5, 0),
(10002, 'Test', 'This is a testing product', 5, 2, 23.43, 43.23, 4, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orderproduct`
--

DROP TABLE IF EXISTS `orderproduct`;
CREATE TABLE IF NOT EXISTS `orderproduct` (
  `orderNR` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(11) NOT NULL,
  `sellingPrice` double NOT NULL,
  PRIMARY KEY (`orderNR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
