﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CoolPurple
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        int? selectedId;
        bool count = true;
        private DBConnectionProduct productconn = new DBConnectionProduct();
        private List<ClassProduct> dbproductlist;

        public Dashboard()
        {
            InitializeComponent();
            dbproductlist = productconn.findAll();
            fillListbox();
            buttonEdit.IsEnabled = false;
            buttonDel.IsEnabled = false;
            buttonEdit.Foreground = Brushes.Gray;
            buttonDel.Foreground = Brushes.Gray;
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            AddWindow addwin = new AddWindow();
            //App.Current.MainWindow = addwin;
            addwin.Show();

        }

        private void buttonDel_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure?", "Are you sure?", MessageBoxButton.YesNo);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    // remove from database
                    ClassProduct prod = new ClassProduct();
                    prod.ClassProductID = (int)selectedId;
                    //check if listbix isnt empty first
                    if (productList.SelectedIndex >= 0)
                    {
                        productList.Items.Remove(productList.SelectedItem);
                        
                        productconn.remove(prod);
                    }

                    //MessageBox.Show("Removed", "Removed");
                    break;
                case MessageBoxResult.No:
                    // not remove from database

                    MessageBox.Show("Not removed", "Not removed");
                    break;
            }
        }

        private void buttonEdit_Click(object sender, RoutedEventArgs e)
        {

            EditWindow editwin = new EditWindow((int)selectedId);
            editwin.Show();
        }

        private void buttonOrder_Click(object sender, RoutedEventArgs e)
        {
            OrderWindow orderwin;
            if (selectedId.HasValue)
                orderwin = new OrderWindow((int)selectedId);
            else
                orderwin = new OrderWindow();
            orderwin.Show();
        }

        public void refillListBox()
        {
            dbproductlist = productconn.findAll();
            fillListbox();
        }

        public void fillListbox()
        {
            productList.Items.Clear();

            //List<ClassProduct> dbproductlist = productconn.findAll();

            foreach (ClassProduct product in this.dbproductlist)
            {
                TextBlock productName = new TextBlock();
                productName.TextWrapping = TextWrapping.Wrap;
                productName.FontWeight = FontWeights.Bold;
                productName.Text =  product.Name; ;

                TextBlock sellingPrice = new TextBlock();
                sellingPrice.Text = "Selling price: " + product.SellingPrice.ToString("0.00") + " euro";

                TextBlock stock = new TextBlock();
                stock.Text = "Stock: " + product.Stock;


                StackPanel sp = new StackPanel();
                sp.Children.Add(productName);
                sp.Children.Add(sellingPrice);
                sp.Children.Add(stock);

                ListBoxItem lbi = new ListBoxItem();

                lbi.Content = sp;
                lbi.Selected += new RoutedEventHandler((sender, e) => {
                    selectedId = product.ClassProductID; 
                }) ;

                productList.Items.Add(lbi);
            }

            warnings();

        }



        private void warnings()
        {

            warningPanel.Children.Clear();

            DBConnectionProduct productconn = new DBConnectionProduct();
            List<ClassProduct> warninglist = productconn.findAllWhereStockIsLow();

            foreach (ClassProduct product in warninglist)
            {
                TextBlock warning = new TextBlock();
                warning.FontWeight = FontWeights.Bold;
                warning.TextWrapping = TextWrapping.Wrap;
                warning.Text = "WARNING!";

                TextBlock productName = new TextBlock();
                productName.TextWrapping = TextWrapping.Wrap;
                productName.Text = product.Name;

                TextBlock aantal = new TextBlock();
                aantal.Text = "In stock: " + product.Stock.ToString();

                Rectangle rec = new Rectangle();
                rec.Height = 10;
                rec.Width = 200;

                SolidColorBrush lightdarkred = new SolidColorBrush();
                lightdarkred = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFB14646"));

                rec.Fill = lightdarkred;
               

                StackPanel stackpanel = new StackPanel();
                stackpanel.Children.Add(warning);
                stackpanel.Children.Add(productName);
                stackpanel.Children.Add(aantal);
                stackpanel.Children.Add(rec);
                stackpanel.HorizontalAlignment = HorizontalAlignment.Left;

                Button button = new Button();

                button.Content = stackpanel;
                button.Padding = new Thickness(10);
                button.Width = 200;
                button.Margin = new Thickness(0,10,0,0);

                button.Click += new RoutedEventHandler((sender, e) => {
                    selectedId = product.ClassProductID;
                });
                button.Click += (s, e) => buttonOrder_Click(s, e);

                warningPanel.Children.Add(button);
            }

           
        }

        private void productList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
            if (count)
            {
                //MessageBox.Show("Double click to for product details!");
                count = false;
            }

            if (productList.SelectedItem != null)
            {
                buttonEdit.IsEnabled = true;
                buttonDel.IsEnabled = true;
                buttonEdit.Foreground = Brushes.White;
                buttonDel.Foreground = Brushes.White;
            }
            else
            {
                buttonEdit.IsEnabled = false;
                buttonDel.IsEnabled = false;
                buttonEdit.Foreground = Brushes.Gray;
                buttonDel.Foreground = Brushes.Gray;
            }
            
        }

        
        private void productList_DoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (productList.SelectedItem != null)
            {
                DetailsWindow detailsWin = new DetailsWindow((int)selectedId);
                detailsWin.Show();

            }
           
        }

        private void SearchQuery_TextChanged(object sender, TextChangedEventArgs e)
        {
            dbproductlist = productconn.searchByQuery(searchQuery.Text);
            fillListbox();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            //do my stuff before closing

            base.OnClosing(e);
            Application.Current.Shutdown();
        }

    }
}
