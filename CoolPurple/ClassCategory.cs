﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolPurple
{
    class ClassCategory
    {
        private int classCategoryID;
        private string name;

        public int ClassCategoryID { get => classCategoryID; set => classCategoryID = value; }
        public string Name { get => name; set => name = value; }

        public override string ToString()
        {
            return this.name;
        }
    }
}
