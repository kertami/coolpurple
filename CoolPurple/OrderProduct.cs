﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolPurple
{
    class OrderProduct
    {
        private int classOrderID;
        private int classProductID;
        private double sellingPrice;

        public int ClassOrderID { get => classOrderID; set => classOrderID = value; }
        public int ClassProductID { get => classProductID; set => classProductID = value; }
        public double SellingPrice { get => sellingPrice; set => sellingPrice = value; }
    }
}
