﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Reflection;

namespace CoolPurple
{
    class DBConnectionProduct : DBConnection<ClassProduct>
    {
        /// <summary>
        /// Find all products where the stock is below minstock
        /// </summary>
        /// <returns></returns>
        public List<ClassProduct> findAllWhereStockIsLow()
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = "SELECT * FROM ClassProduct WHERE Stock < minStock";

            List<ClassProduct> returnList = new List<ClassProduct>();

            MySqlDataReader result = Database.getInstance().executeQuery(cmd);

            while (result.Read())
            {
                ClassProduct obj = new ClassProduct();
                foreach (PropertyInfo propertyInfo in typeof(ClassProduct).GetProperties())
                {
                    propertyInfo.SetValue(obj, result[propertyInfo.Name]);
                }
                returnList.Add(obj); 
            }

            result.Close();

            return returnList;
        }


        public List<ClassProduct> searchByQuery(string query)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = "SELECT * FROM ClassProduct AS cp " +
                "LEFT JOIN ClassCategory AS cc ON cp.CategoryID = cc.ClassCategoryID " +
                "WHERE lower(cp.NAME) like @query " +
                "OR lower(cp.Description) like @query " +
                "OR lower(cc.name) like @query";

            cmd.Parameters.AddWithValue("@query", "%"+query.ToLower()+"%");

            List<ClassProduct> returnList = new List<ClassProduct>();

            MySqlDataReader result = Database.getInstance().executeQuery(cmd);

            while (result.Read())
            {
                ClassProduct obj = new ClassProduct();
                foreach (PropertyInfo propertyInfo in typeof(ClassProduct).GetProperties())
                {
                    propertyInfo.SetValue(obj, result[propertyInfo.Name]);
                }
                returnList.Add(obj);
            }

            result.Close();

            return returnList;
        }
    }

}
