﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolPurple
{
    class ClassOrder
    {
        private int classOrderID;
        private string date;
        private double total;

        public int ClassOrderID { get => classOrderID; set => classOrderID = value; }
        public string Date { get => date; set => date = value; }
        public double Total { get => total; set => total = value; }

    }
}
