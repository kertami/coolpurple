﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolPurple
{
    class ClassProduct
    {
        private int classProductID;
        private String name;
        private int categoryID;
        private String description;
        private int stock;
        private double purchasePrice;
        private double sellingPrice;
        private int minStock;
        private int maxStock;

        public int ClassProductID { get => classProductID; set => classProductID = value; }
        public string Name { get => name; set => name = value; }
        public int CategoryID { get => categoryID; set => categoryID = value; }
        public string Description { get => description; set => description = value; }
        public int Stock { get => stock; set => stock = value; }
        public double PurchasePrice { get => purchasePrice; set => purchasePrice = value; }
        public double SellingPrice { get => sellingPrice; set => sellingPrice = value; }
        public int MinStock { get => minStock; set => minStock = value; }
        public int MaxStock { get => maxStock; set => maxStock = value; }

        public override string ToString()
        {
            return name;
        }
    }
}
