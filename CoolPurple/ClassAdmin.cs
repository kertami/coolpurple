﻿
namespace CoolPurple
{
    class ClassAdmin
    {
        private int classAdminID;
        private string username;
        private string password;

        public int ClassAdminID { get => classAdminID; set => classAdminID = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }

        
    }
}
