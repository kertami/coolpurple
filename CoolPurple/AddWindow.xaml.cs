﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace CoolPurple
{
    /// <summary>
    /// Interaction logic for AddWindow.xaml
    /// </summary>
    public partial class AddWindow : Window
    {

        private DBConnectionCategory dbconcat = new DBConnectionCategory();

        public AddWindow()
        {
            InitializeComponent();

            List<ClassCategory> categories = dbconcat.findAll();

            foreach (ClassCategory cat in categories)
            {
                System.Windows.Controls.ComboBoxItem cbi = new System.Windows.Controls.ComboBoxItem();
                cbi.Tag = cat.ClassCategoryID;
                cbi.Content = cat;

                ComboBoxCategories.Items.Add(cbi);
            }

            
        }

        private void buttonAddThis_Click(object sender, RoutedEventArgs e)
        {
            ClassProduct product = new ClassProduct();
            
            product.Name = textBoxName.Text;

            int categoryID;
            ComboBoxItem selected = (ComboBoxItem) ComboBoxCategories.SelectedItem;
            if (selected == null)
            {
                if (MessageBox.Show("Category: \'" + ComboBoxCategories.Text + "\' does not exist, add it?", "Warning", MessageBoxButton.YesNo) == MessageBoxResult.Yes )
                {
                    ClassCategory cat = new ClassCategory();
                    cat.Name = ComboBoxCategories.Text;

                    categoryID = dbconcat.insert(cat);
                    if (categoryID == 0)
                    {
                        MessageBox.Show("something went wrong :(");
                        return;
                    }
                } else
                {
                    return;
                }
                
            } else
            {
                int.TryParse(selected.Tag.ToString(), out categoryID);
            }

            product.CategoryID = categoryID;

            double purchasePrice;
            double.TryParse(TextBoxPurchasePrice.Text, out purchasePrice);
            product.PurchasePrice = purchasePrice;

            double sellingPrice;
            double.TryParse(TextBoxSellingPrice.Text, out sellingPrice);
            product.SellingPrice = sellingPrice;

            int minStock;
            int.TryParse(TextboxMinStock.Text, out minStock);
            product.MinStock = minStock;

            int maxStock;
            int.TryParse(TextboxMaxStock.Text, out maxStock);
            product.MaxStock = maxStock;

            DBConnectionProduct dbConnectionProduct = new DBConnectionProduct();
            dbConnectionProduct.insert(product);

            Dashboard dash = (Dashboard)Application.Current.MainWindow;
            dash.refillListBox();
            this.Close();
        }
    }
}
