﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CoolPurple
{
    class DBConnectionClassAdmin : DBConnection<ClassAdmin>
    {
        public string getPassword(string username)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = "SELECT * FROM ClassAdmin WHERE username = @username LIMIT 1";
            cmd.Parameters.AddWithValue("@username", username);

            MySqlDataReader result = Database.getInstance().executeQuery(cmd);

            ClassAdmin obj = new ClassAdmin();

            while (result.Read())
            {
                foreach (PropertyInfo propertyInfo in typeof(ClassAdmin).GetProperties())
                {
                    propertyInfo.SetValue(obj, result[propertyInfo.Name]);
                }
                
            }

            result.Close();
            return obj.Password;

        }

    }
}
