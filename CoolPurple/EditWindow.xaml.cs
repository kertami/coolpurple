﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CoolPurple
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        private DBConnectionProduct dbconprot;
        private DBConnectionCategory dbconcat;

        private ClassProduct product;
        private List<ClassCategory> categories;
        public EditWindow(int id)
        {
            InitializeComponent();
            dbconprot = new DBConnectionProduct();
            dbconcat = new DBConnectionCategory();
            product = dbconprot.find(id);
            categories = dbconcat.findAll();

            textBoxName.Text = product.Name;
            //TextBoxCategory.Text = product.CategoryID + "";//cheating
            foreach (ClassCategory cat in categories)
            {
                ComboBoxItem cbi = new ComboBoxItem();
                cbi.Tag = cat.ClassCategoryID;
                cbi.Content = cat;
                if (cat.ClassCategoryID.Equals(product.CategoryID))
                {
                    cbi.IsSelected = true;
                }

                ComboBoxCategories.Items.Add(cbi);
            }
            TextBoxStock.Text = product.Stock + "";
            TextBoxPurchasePrice.Text = product.PurchasePrice + "";
            TextBoxSellingPrice.Text = product.SellingPrice + "";
            TextboxMinStock.Text = product.MinStock + "";
            TextboxMaxStock.Text = product.MaxStock + "";
        }

        // the stuff has to be in the textboxes allready

        private void buttonEditThis_Click(object sender, RoutedEventArgs e)
        {
            // update stuff
            product.Name = textBoxName.Text;

            int categoryID;
            ComboBoxItem selected = (ComboBoxItem)ComboBoxCategories.SelectedItem;
            if (selected == null)
            {
                if (MessageBox.Show("Category: \'" + ComboBoxCategories.Text + "\' does not exist, add it?", "Warning", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    ClassCategory cat = new ClassCategory();
                    cat.Name = ComboBoxCategories.Text;

                    categoryID = dbconcat.insert(cat);
                    if (categoryID == 0)
                    {
                        MessageBox.Show("something went wrong :(");
                        return;
                    }
                }
                else
                {
                    return;
                }

            }
            else
            {
                int.TryParse(selected.Tag.ToString(), out categoryID);
            }
            product.CategoryID = categoryID;

            double purchasePrice;
            double.TryParse(TextBoxPurchasePrice.Text, out purchasePrice);
            product.PurchasePrice = purchasePrice;

            int stock;
            int.TryParse(TextBoxStock.Text, out stock);
            product.Stock = stock;

            double sellingPrice;
            double.TryParse(TextBoxSellingPrice.Text, out sellingPrice);
            product.SellingPrice = sellingPrice;

            int minStock;
            int.TryParse(TextboxMinStock.Text, out minStock);
            product.MinStock = minStock;

            int maxStock;
            int.TryParse(TextboxMaxStock.Text, out maxStock);
            product.MaxStock = maxStock;

            dbconprot.update(product);

            Dashboard dash = (Dashboard)Application.Current.MainWindow;
            dash.refillListBox();
            this.Close();
        }
    }
}
