﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace CoolPurple
{
    /// <summary>
    /// Interaction logic for OrderWindow.xaml
    /// </summary>
    public partial class OrderWindow : Window
    {
        private DBConnectionProduct dbconprod;
        private List<ClassProduct> products;
        private ClassProduct product = null;

        public OrderWindow()
        {
            init();
            product = new ClassProduct();
            fillList();
        }

        public OrderWindow(int id)
        {
            init();
            product = dbconprod.find(id);
            fillList();
        }

        private void init()
        {
            dbconprod = new DBConnectionProduct();
            products = dbconprod.findAll();
            InitializeComponent();
        }

        private void fillList()
        {
            foreach (ClassProduct prod in products)
            {
                ComboBoxItem cbi = new ComboBoxItem();
                cbi.Tag = prod.ClassProductID;
                cbi.Content = prod;

                if (prod.ClassProductID == product.ClassProductID)
                    cbi.IsSelected = true;

                ComboBoxProducts.Items.Add(cbi);
            }
        }

        private void ButtonAddThis_Click(object sender, RoutedEventArgs e)
        {
            int id;
            ComboBoxItem selected = (ComboBoxItem)ComboBoxProducts.SelectedItem;
            if (selected == null)
            {
                MessageBox.Show("Product not found, please try again.", "Warning");
                return;
            }

            int.TryParse(selected.Tag.ToString(), out id);
            product.ClassProductID = id;

            int aantal;
            int.TryParse(TextBoxAantal.Text, out aantal);
            ClassProduct order = dbconprod.find(id);

            if (order.Stock+aantal > order.MaxStock)
            {
                MessageBox.Show("This product has a maximum stock of "+order.MaxStock+", current stock is "+order.Stock+". Please try again.", "Warning");
                return;
            }
            order.Stock += aantal;

            ClassOrder classOrder = new ClassOrder();

            classOrder.Date = System.DateTime.Now.ToString();
            classOrder.Total = order.PurchasePrice;

            DBConnectionOrder dbconOrd = new DBConnectionOrder();
            int coID = dbconOrd.insert(classOrder);

            DBConnectionOrderProduct dbconOP = new DBConnectionOrderProduct();
            for (int i = 0; i < aantal; i++) { 
                OrderProduct ordProd = new OrderProduct();
                ordProd.ClassProductID = order.ClassProductID;
                ordProd.ClassOrderID = coID;
                ordProd.SellingPrice = order.SellingPrice;
                dbconOP.insert(ordProd);
            }

            dbconprod.update(order);

            Dashboard dash = (Dashboard)Application.Current.MainWindow;
            dash.refillListBox();
            this.Close();
        }
        
    }
}
