﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace CoolPurple
{
    /// <summary>
    /// Logical link to the database.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    abstract class DBConnection<T> where T : new()
    {

        protected String tableName, tableID;
        private Type workingClass;

        public DBConnection()
        {
            workingClass = typeof(T);
            tableName = workingClass.Name;
            tableID = tableName + "ID"; 
        }


        /// <summary>
        /// Retrieve a single object of type by id
        /// </summary>
        /// <param name="id">object id to look for in the database</param>
        /// <returns>requested object if found</returns>
        public T find(object id)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = "SELECT * FROM " + tableName + " WHERE " + tableID + " = @id";
            cmd.Parameters.AddWithValue("@id", id);

            MySqlDataReader result = Database.getInstance().executeQuery(cmd);

            T obj = new T();
            while (result.Read())
            {
                foreach (PropertyInfo propertyInfo in workingClass.GetProperties())
                {
                    propertyInfo.SetValue(obj, result[propertyInfo.Name]);
                }
            }

            result.Close();

            return obj;
        }

        /// <summary>
        /// Retrieve all objects of type from database.
        /// </summary>
        /// <returns>list of object type</returns>
        public List<T> findAll()
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = "SELECT * FROM " + tableName + " LIMIT 50";

            List<T> returnList = new List<T>();

            MySqlDataReader result = Database.getInstance().executeQuery(cmd);

            while (result.Read())
            {
                T obj = new T();
                foreach (PropertyInfo propertyInfo in workingClass.GetProperties())
                {
                    propertyInfo.SetValue(obj, result[propertyInfo.Name]);
                }
                returnList.Add(obj);
            }

            result.Close();

            return returnList;
        }

        /// <summary>
        /// Insert new object into database
        /// </summary>
        /// <param name="entity">The object to add</param>
        /// <returns>Returns ID of inserted object</returns>
        public int insert(T entity)
        {
            MySqlCommand cmd = new MySqlCommand();
            string commandString = "INSERT INTO " + tableName;

            string valueNames = " (";
            string values = "VALUES (";
            for (int i = 0; i < entity.GetType().GetProperties().Length; i++)
            {
                PropertyInfo propertyInfo = entity.GetType().GetProperties()[i];
                valueNames += propertyInfo.Name;
                values += "'"+entity.GetType().GetProperty(propertyInfo.Name).GetValue(entity, null)+"'";

                if (i < entity.GetType().GetProperties().Length - 1)
                {
                    valueNames += ", ";
                    values += ", ";
                }                    
            }
            valueNames += ") ";
            values += "); SELECT LAST_INSERT_ID();";

            commandString += valueNames + values;
            

            cmd.CommandText = commandString;

            int result = Database.getInstance().executeScalar(cmd);

            return result;
        }

        /// <summary>
        /// Remove given object from database
        /// </summary>
        /// <param name="entity">The object you'd like to remove</param>
        /// <returns>returns true if removed succesfully</returns>
        public bool remove(T entity)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = "DELETE FROM " + tableName + " WHERE "+tableID + " = @id";

            cmd.Parameters.AddWithValue("@id", entity.GetType().GetProperty(tableID).GetValue(entity, null));

            int result = Database.getInstance().executeNonQuery(cmd);

            return result != 0 ? true : false;
        }

        /// <summary>
        /// Edit object from database
        /// </summary>
        /// <param name="entity">The object to update</param>
        /// <returns></returns>
        public bool update(T entity)
        {
            MySqlCommand cmd = new MySqlCommand();
            string commandString = "UPDATE " + tableName + " SET ";

            string values = "";
            for (int i = 0; i < entity.GetType().GetProperties().Length; i++)
            {
                PropertyInfo propertyInfo = entity.GetType().GetProperties()[i];
                values += propertyInfo.Name + " = ";
                values += "'" + entity.GetType().GetProperty(propertyInfo.Name).GetValue(entity, null) + "'";

                if (i < entity.GetType().GetProperties().Length - 1)
                {
                    values += ", ";
                }


            }

            commandString += values;

            commandString += " WHERE " + tableID + " = @id";

            cmd.CommandText = commandString;
            cmd.Parameters.AddWithValue("@id", entity.GetType().GetProperty(tableID).GetValue(entity, null));


            int result = Database.getInstance().executeNonQuery(cmd);

            return result != 0 ? true : false;
        }


    }
}