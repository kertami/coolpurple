﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace CoolPurple
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            
        }

        private void buttonLogin_Click(object sender, RoutedEventArgs e)
        {
            //first check if login is rite

            Dashboard dashboard = new Dashboard();
            App.Current.MainWindow = dashboard;

            // check account 
            // select password from Admin where username = ?
            // 

            string username = textBoxUsername.Text;
            string password = TextBoxPassword.Password;
            DBConnectionClassAdmin dbcon = new DBConnectionClassAdmin();

            string dbpassword = dbcon.getPassword(username);

            if (password.Equals(dbpassword))
            {
                ClassAdmin admin = new ClassAdmin();
                admin.Username = username;

                this.Close();
                dashboard.Show();
            } else
            {
                lbl_error.Content = "Failed to login.";
            }

        }
    }
}
