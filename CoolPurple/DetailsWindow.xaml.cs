﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CoolPurple
{
    /// <summary>
    /// Interaction logic for Details.xaml
    /// </summary>
    public partial class DetailsWindow : Window
    {
        private DBConnectionProduct dbproduct;
        private DBConnectionCategory dbcategory;
        private ClassProduct product;
        private ClassCategory category;

        public DetailsWindow(int id)
        {
            InitializeComponent();
            dbproduct = new DBConnectionProduct();
            product = dbproduct.find(id);

            dbcategory = new DBConnectionCategory();
            category = dbcategory.find(product.CategoryID);


            // Maak een stringbuilder en voeg product details toe;
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append("1. Name of The Product: " + product.Name + "\n");
            strBuilder.Append("2. Category: " + category + "\n");
            strBuilder.Append("3. Stock: " + product.Stock + "\n");
            strBuilder.Append("4. PurchasePrice: " + product.PurchasePrice + "\n");
            strBuilder.Append("5. SellingPrice: " + product.SellingPrice + "\n");
            strBuilder.Append("6. MiniStock: " + product.MinStock + "\n");
            strBuilder.Append("7. MaxStock: " + product.MaxStock + "");

            //voeg stringbuilder in textBox toe;
            textProduct.Text = strBuilder.ToString();
            textProduct.FontWeight = FontWeights.Bold;
            textProduct.Background = Brushes.AliceBlue;
        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
